<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
session_start();

if (isset($_GET['action']) && $_GET['action'] == "signOut") {
	session_destroy();
} 

?>

<!DOCTYPE html>
<html>

<?php if(isset($_SESSION['player'])) : ?>
	<p class="welcome">Welcome <?php echo $_SESSION['player']['player_name'] ?>  </p>
	<div class="wrong-user">not <?php echo $_SESSION['player']['player_name'] ?>?</div>
	<a href="index.php?action=signOut"> sign out</a>
	<br/><br/>
	<?php if($_SESSION['player']['is_admin'] == 1) : ?>
		<p class="admin">You are an admin</p>
	<?php endif; ?>
	<?php if(isset($_SESSION['deck'])) : ?>
		<a href="play.php" class="play">Play Game!</a>
		<p class="deck-name">Your are using deck 
			<?php echo $_SESSION['deck']['deck_name'] ?></p>
	<?php endif; ?>
	<a href="deck">select deck</a>

<?php else : ?>
	<a href="account/sign-in.php"> sign in</a>
	<a href="account/sign-up.php">sign up</a>

<?php endif; ?>
</html>