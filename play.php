<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
session_start();

if (isset($_GET['action']) && $_GET['action'] == "getCards") {
	echo json_encode($_SESSION['deck']['cards']);
	exit();
}
?>


<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="js/jquery-mobile/jquery.mobile.custom.min.js"></script>
	<script type="text/javascript" src="js/card.js"></script>
	<script type="text/javascript" src="js/game_driver.js"></script>
	<link rel="stylesheet" type="text/css" href="js/jquery-mobile/jquery.mobile.custom.structure.min.css" media="screen and (orientation: landscape)"/>
	<link rel="stylesheet" type="text/css" href="js/jquery-mobile/jquery.mobile.custom.theme.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body>

<button id="begin-game_one" class="begin-game">Begin Game as player one</button>
<button id="begin-game_two" class="begin-game">Begin Game as player two</button>
<div class="end-game hide"></div>
<div id="game-board" class="hide">
	<div class="status-spacer"></div>
	<div class="outer-hand card-row">
		<div id="player-two-hand"></div>
	</div>
	<div id="player-two-player" class="card-row">
		<div id="player-two-mech" class="player-lane">
			<div id="player-two" class="card player-card">
				<div class="card-top-row">
					<div class="attack"></div>
					<div class="debt"></div>
					<div class="health"></div>
				</div>
				<div class="name"></div>
				<div class="bank"></div>
			</div>
		</div>
		<div id="player-two-clinic" class="player-lane">
		</div>
		<div id="player-two-building">
			<div class="card building-card float-right">
				<div class="card-top-row">
					<div class="progress"></div>
					<div class="cost"></div>
				</div>
				<div class="name"></div>
				<div class="description hide"></div>
				<div class="play hide">Activate</div>
			</div>
		</div>
	</div>
	<div id="player-two-placement" class="card-row">
		<div id="player-two-left" class="lane"></div>
		<div id="player-two-center" class="lane"></div>
		<div id="player-two-right"class="lane"></div>
	</div>
	<div id="player-one-placement" class="card-row">
		<div id="player-one-left" class="lane"></div>
		<div id="player-one-center" class="lane"></div>
		<div id="player-one-right" class="lane"></div>
	</div>
	<div id="player-one-player" class="card-row">
		<div id="player-one-mech" class="player-lane">
			<div id="player-one" class="card player-card">
				<div class="card-top-row">
					<div class="attack"></div>
					<div class="debt"></div>
					<div class="health"></div>
				</div>
				<div class="name"></div>
				<div class="bank"></div>
			</div>
		</div>
		<div id="player-one-clinic" class="player-lane">
		</div>
		<div id="player-one-building">
			<div class="card building-card float-right">
				<div class="card-top-row">
					<div class="progress"></div>
					<div class="cost"></div>
				</div>
				<div class="name"></div>
				<div class="description hide"></div>
				<div class="play hide">Activate</div>
			</div>
		</div>
	</div>
	<div class="outer-hand card-row">
		<div id="player-one-hand"></div>
	</div>
	<div class="status">
		Turn 
		<span class="current-turn">1</span>
		 | Current Player: 
		<span class="player-name">one</span>
		 | Current Phase: 
		<span class="phase">Main Phase</span>
		<div class="confirm-attack hide">
			<button class="confirm-attack-button action-button">
				Confirm Battle
			</button>
		</div>
		<div class="end-turn">
			<button class="end-turn-button action-button"> End
				<span class="phase">
					Turn
				</span>
			</button>
		</div>
		<div class="attack-turn hide">
			<button class="attack-button action-button">
				<span class="phase">
					Attack
				</span>
			</button>
		</div>
		<div class="cancel-action hide">
			<button class="cancel-action-button action-button">
				Cancel Action
			</button>
		</div>
	</div>
	<div class="showing-combat-results hide">Showing Combat Results</div>
	
</div>
<div id="zoom-card-display" class="hide"></div>
</body>
</html>