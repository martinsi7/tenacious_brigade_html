<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

function create_connection(){
	$configs = include(dirname(__FILE__) . "/../../config/connection.php");
	// Create connection
	$conn = new mysqli($configs['host'],
		$configs['username'],
		$configs['password'],
		$configs['database']);

	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}

	return $conn;
}

function db_insert($table_name, $fields, $values) {
	$conn = create_connection();
	$safe_table = $conn->real_escape_string($table_name);
	$sql = "insert into $safe_table (";
	foreach ($fields as $field) {
		$safe_field = $conn->real_escape_string($field);
		$sql .= "$safe_field,";
	}
	$sql = rtrim($sql, ",");
	$sql .= ") values";
	foreach ($values as $value_set) {
		$sql .= " (";
		foreach ($value_set as $value) {
			$safe_value = $conn->real_escape_string($value);
			if (is_null($safe_value)) {
				$sql .= "null,";
			} elseif (is_string($safe_value)) {
				$sql .= "'$safe_value',";
			} elseif (is_numeric($safe_value)) {
				$sql .= "$safe_value,";
			} else {
				$sql .= "null,";
			}
		}
		$sql = rtrim($sql, ",");
		$sql .= "),";
	}
	$sql = rtrim($sql, ",");
	$sql .= ";";
	//echo $sql;
	if (!$conn->query($sql)) {
	    echo "Error: " . $sql . "<br>" . $conn->error;
	    return FALSE;
	}
	return TRUE;
}

function db_get_player($email) {
	$conn = create_connection();
	$safe_email = $conn->real_escape_string($email);
	$sql = "select * from Player where email='$email'";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) {
	        return $row;
	    }
	} else {
	    return NULL;
	}
}

function db_select($field_str, $conditional_str) {
	$conn = create_connection();
	$sql = "select $field_str $conditional_str";
	$result = $conn->query($sql);
	$ret_array = array();
	while($row = $result->fetch_assoc()) {
        array_push($ret_array, $row);
    }
    return $ret_array;
}

function get_decks($player_id) {
	$conn = create_connection();
	$safe_player = $conn->real_escape_string($player_id);
	$sql = "select deck_name, deck_id from Deck where player_id='$safe_player'";
	$result = $conn->query($sql);
	$ret_array = array();
	while($row = $result->fetch_assoc()) {
        array_push($ret_array, $row);
    }
    return $ret_array;
}

function db_get_deck_from_name($player_id, $deck_name) {
	$conn = create_connection();
	$safe_player = $conn->real_escape_string($player_id);
	$safe_deck = $conn->real_escape_string($deck_name);
	$sql = "select deck_id from Deck
		where player_id='$safe_player' and deck_name='$safe_deck'";
	$result = $conn->query($sql);
	$deck_id = null;
	while($row = $result->fetch_assoc()) {
        $deck_id = $row['deck_id'];
        break;
    }
    $cards = db_get_deck_cards($deck_id);
    return array(
    	"deck_id" => $deck_id,
    	"deck_name" => $deck_name,
    	"cards" => $cards
    );
}

function db_get_deck_cards($deck_id) {
	$conn = create_connection();
	$safe_deck = $conn->real_escape_string($deck_id);
	$sql = "select c.card_name from Card c, DeckList dl
	where dl.deck_id=$safe_deck and c.card_id=dl.card_id";
	$cards = array();
	$result = $conn->query($sql);
	while($row = $result->fetch_assoc()) {
        array_push($cards, ($row['card_name']));
    }
    return $cards;
}

function get_collection($player_id, $is_admin, $faction) {
	$conn = create_connection();
	$safe_player = $conn->real_escape_string($player_id);
	$sql = "";
	if (TRUE /*$is_admin*/ ) {
		$sql = "select * from Card";
	} else {
		// TODO: implement join on collection table to get cards for normal players
	}
	if ($faction != null) {
		$safe_faction = $conn->real_escape_string($faction);
		$sql .= " where Card.faction='$safe_faction' or Card.faction='Neutral'";
	}
	$sql .= " order by cost, card_name";
	
	$result = $conn->query($sql);
	$building_array = array();
	$mech_array = array();
	$unit_array = array();

	while($row = $result->fetch_assoc()) {
		if ($row['card_type'] == 'building') {
			array_push($building_array, $row);
		} elseif ($row['card_type'] == 'mech') {
			array_push($mech_array, $row);
		} else {
			array_push($unit_array, $row);
		}
        
    }
    return array($building_array, $mech_array, $unit_array);
}

?>