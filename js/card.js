CONTROLLER_URL = "server_interface.php"
PLAYER_NAME = ""

$(document).ready(function(){
	calculateZoomDivPos();
	$(this).on("dblclick", ".card", function(){
		handleCardDoubleClicks($(this));
	});
	$(this).on("orientationchange", function() {
		alert("yay!");
	})
	$(document).on("taphold", ".card", function(){
		handleCardDoubleClicks($(this));
	});
	
	$(this).on("click", ".card", function(){
		handleCardClicks($(this));
	});
	$(".confirm-attack").click(function(){
		doConfirmCombatFunc();
	});
})

function calculateZoomDivPos() {
	var zoom = $("#zoom-card-display");
	var left = ($(window).width() - zoom.width()) / 2;
	//var top = ($(window).height() - zoom.height()) / 2;
	left = (left < 0) ? 0 : left;
	//top = (top < 0) ? 0 : top;
	zoom.css({
		"left": left + "px"
	});
}

var deckChoices = [
	"Deck One",
	"Deck Two",
	"The Ole Switcheroo",
	"Might of Oaks",
	"Time to Go Mobile"
];

allCards = {};
allPlayers = {};
highlightedCard = null;
multipleTargets = [];
gamePhase = "firstMain";

var doNothing = function(){}

var GameState = function(){
	this.playerOneHand = []
	this.playerOneMech = []
	this.playerOneLeft = []
	this.playerOneCenter = []
	this.playerOneRight = []

	this.playerTwoHand = []
	this.playerTwoMech = []
	this.playerTwoLeft = []
	this.playerTwoCenter = []
	this.playerTwoRight = []

}

var CardInfo = function(cardInfo, index) {
	this.id = cardInfo.gameID;
	this.name = cardInfo.name;
	this.attack = cardInfo.attack;
	this.health = cardInfo.health;
	this.zone = cardInfo.zone;
	this.description = cardInfo.description;
	this.cooldown = cardInfo.cooldown;
	this.isPrepTime = cardInfo.isPrepTime
	this.cost = cardInfo.cost;
	this.index = index;
	this.faction = cardInfo.faction;
	this.isInPlay = cardInfo.isInPlay;
	this.abilities = cardInfo.abilities;
	this.cardType = cardInfo.cardType;
	this.mobility = cardInfo.mobility;
	this.canMove = cardInfo.canMove;
	this.unitType = cardInfo.unitType;
	this.extortion = cardInfo.extortion;
	this.aggressive = cardInfo.aggressive;
	this.defensive = cardInfo.defensive;
	this.reach = cardInfo.reach;
	this.tenacity = cardInfo.tenacity;
	this.regeneration = cardInfo.regeneration;
	this.shambling = cardInfo.shambling;
	this.mechanized = cardInfo.mechanized;
}

var PlayerInfo = function(playerObj) {
	this.name = playerObj.name;
	this.health = playerObj.life;
	this.bank = playerObj.bank;
	this.attack = playerObj.attack
	this.debt = playerObj.debt;
	this.canAttack = playerObj.canAttack;
	this.selectorPrefix = (playerObj.name == "one") ? "#player-one-" : "#player-two-";
	this.building = new BuildingInfo(playerObj.building)
}

var BuildingInfo = function(buildingObj) {
	this.id = buildingObj.gameID;
	this.name = buildingObj.name;
	this.description = buildingObj.description;
	this.progress = buildingObj.progress;
	this.ready = buildingObj.ready;
	this.deployed = buildingObj.deployed;
	this.cost = buildingObj.cost;

}



var zoomedCardID = null;

function handleCardDoubleClicks(cardObj) {
	zoomedCardID = cardObj.attr("id");
	var newCard = cardObj.clone();
	var zoom = $("#zoom-card-display")
	newCard.find(".description").removeClass("hide");
	newCard.find(".abilities").removeClass("hide");
	newCard.find(".change-lane").removeClass("hide");
	zoom.html(newCard);
	zoom.removeClass("hide");
	//var newTop = ($("body").scrollTop() + zoom.position().top) + "px";
	//zoom.css("top", newTop);
	$("#game-board").fadeTo("fast", .5);
	$("#game-board").one("click", function(){
		unZoom();
	})
}

function unZoom() {
	var zoom = $("#zoom-card-display")
	zoom.addClass("hide");
	calculateZoomDivPos();
	$("#game-board").fadeTo("fast", 1);
	zoomedCardID = null;
}

function handleCardClicks(cardObj){
	if (cardObj.hasClass("active")){
		$(".active").removeClass("highlighted")
		$(".active-mulitple").removeClass("highlighted")
		cardObj.addClass("highlighted")
		highlightedCard = cardObj;
		doActiveCardFunc(cardObj);
		(function(cardObj) {
			$("body").one("click", function(){
				cardObj.removeClass("highlighted");
			});
		})(cardObj)
	} else if (cardObj.hasClass("active-multiple")){
		if (cardObj.hasClass("highlighted")){
			cardObj.removeClass("highlighted");
			undoActiveMultipleFunc(cardObj);
		} else {
			cardObj.addClass("highlighted");
			doActiveMultipleFunc(cardObj);
		}
	}
}

var doActiveCardFunc = function(cardObj) {
	console.log("hi there")
}

var doActiveMultipleFunc = function(cardObj) {
	console.log("lots of hi there");
}

var undoActiveMultipleFunc = function(){
	console.log("no more hi there");
}

var doConfirmCombatFunc = function(){
	console.log("confirming some combat");
}

/************************************************
 *
 * controller callbacks
 *
 ***********************************************/

function updateCards(cardInfos) {
	for (var index in cardInfos) {
		var cardInfo = cardInfos[index];
		var newCardInfo = new CardInfo(cardInfo);
		var card;
		if (!(newCardInfo.id in allCards)) {
			card = instantiateCard(newCardInfo);
			$("#" + newCardInfo.zone).append(card);
		} else {
			card = $("#" + newCardInfo.id);
			if (newCardInfo.zone != allCards[newCardInfo.id].zone) {
				card.detach().appendTo("#" + newCardInfo.zone);
			}
		}
		allCards[newCardInfo.id] = newCardInfo;
		udpateCardsAttributes(card, newCardInfo);
	}
}

function updatePlayers(playerInfos) {
	for (var index in playerInfos) {
		playerInfo = playerInfos[index];
		if (playerInfo.isDefeated) {
			playerLoss(playerInfo.name);
		}
		if (!(playerInfo.name in allPlayers)){
 		var newInfo = new PlayerInfo(playerInfo);
 		allPlayers[playerInfo.name] = newInfo;
 	} else {
 		player = allPlayers[playerInfo.name];
 		player.bank = playerInfo.bank;
 		player.health = playerInfo.life;
 		player.attack = playerInfo.attack;
 		player.debt = playerInfo.debt;
 		player.canAttack = playerInfo.canAttack;
 		player.building = new BuildingInfo(playerInfo.building);
 	}
		updatePlayerAttributes(allPlayers[playerInfo.name]);
	}
}

function instantiateCard(cardInfo){
	var card = $("<div>", {id: cardInfo.id, class: "card"});
	var topRow = $("<div>").addClass("card-top-row");
	topRow.append($("<div>").addClass("attack"));
	topRow.append($("<div>").addClass("cost"));
	topRow.append($("<div>").addClass("health"));
	card.append(topRow);
	card.append($("<div>").addClass("name"));
	card.append($("<div>").addClass("description").addClass("hide"));
	card.append($("<div>").addClass("abilities").addClass("hide"));
	var $status = $("<div>").addClass("status-icons");
	card.append($status);
	card.append($("<div>").addClass("change-lane hide"));
	addAbilities(cardInfo, card);
	if (cardInfo.cardType == "mech") {
		card.addClass("mech-type");
	}
	return card;
}


function addAbilities(cardInfo, card) {
	for (var i in cardInfo.abilities) {
		ability = cardInfo.abilities[i];
		$ability = $("<div>", {
			class: "ability",
			text: ability
		});
		card.find(".abilities").append($ability);
	}
}

function udpateCardsAttributes(card, cardInfo) {
	card.find(".attack").text(cardInfo.attack);
	card.find(".cost").text("$" + cardInfo.cost);
	card.find(".name").text(cardInfo.name);
	card.find(".health").text(cardInfo.health);
	card.find(".description").html(cardInfo.description);
	if (cardInfo.canMove) {
		card.find(".change-lane").text("Move Unit ->");
	} else {
		card.find(".change-lane").text("");
	}
	updateStatusIcons(card, cardInfo);
}

function updateStatusIcons(card, cardInfo) {
	card.find(".status-icons").html("");
	if (cardInfo.unitType) {
		var $unitTypeImage = $("<div>");
		if (cardInfo.unitType == "specialist") {
			$unitTypeImage.addClass("specialist-image")
		} else if (cardInfo.unitType == "fighter") {
			$unitTypeImage.addClass("fighter-image")
		}
		card.find(".status-icons").append($unitTypeImage);
	}
	if (cardInfo.cooldown && cardInfo.cooldown > 0) {
		var $cooldownImage = $("<div>", {
	 		class: "cooldown-image"
	 	});
	 	var $cooldownAmount = $("<div>", {
	 		class: "cooldown-amount",
	 		text: cardInfo.cooldown
	 	});
	 	card.find(".status-icons").append($cooldownImage);
	 	card.find(".status-icons").append($cooldownAmount);
	}
	if (cardInfo.aggressive) {
		card.find(".status-icons").append(
			$("<div>", {
				class: "aggressive-image"
			})
		);
	}
	if (cardInfo.defensive) {
		card.find(".status-icons").append(
			$("<div>", {
				class: "defensive-image"
			})
		);
	}
	if (cardInfo.mobility) {
		card.find(".status-icons").append(
			$("<div>", {
				class: "mobility-image"
			})
		);
	}
	if (cardInfo.reach) {
		card.find(".status-icons").append(
			$("<div>", {
				class: "reach-image"
			})
		);
	}
	if (cardInfo.regeneration) {
		card.find(".status-icons").append(
			$("<div>", {
				class: "regeneration-image"
			})
		);
	}
	if (cardInfo.shambling) {
		card.find(".status-icons").append(
			$("<div>", {
				class: "shambling-image"
			})
		);
	}
	if (cardInfo.tenacity) {
		card.find(".status-icons").append(
			$("<div>", {
				class: "tenacity-image"
			})
		);
	}
	if (cardInfo.mechanized) {
		console.log("hey!");
		card.find(".status-icons").append(
			$("<div>", {
				class: "mechanized-image"
			})
		);
	}
}

function updatePlayerAttributes(playerInfo){
	var prefix = playerInfo.selectorPrefix;
	var container = $(prefix + "mech .player-card");
	container.find(".name").text(playerInfo.name);
	container.find(".bank").text("$" + playerInfo.bank);
	container.find(".health").text(playerInfo.health);
	container.find(".attack").text(playerInfo.attack);
	if (playerInfo.debt > 0) {
		container.find(".debt").text(playerInfo.debt);
	} else {
		container.find(".debt").text("");
	}
	updateBuildingAttributes(playerInfo);
}

function updateBuildingAttributes(playerInfo) {
	var buildingInfo = playerInfo.building;
	var prefix = playerInfo.selectorPrefix;
	var $container = $(prefix + "building .building-card");
	$container.find(".name").text(buildingInfo.name);
	$container.find(".cost").text("$" + buildingInfo.cost);
	$container.find(".progress").text(buildingInfo.progress + "/10");
	$container.find(".description").text(buildingInfo.description);
	$container.attr("id", buildingInfo.id);
	if (buildingInfo.deployed) {
		$container.removeClass("building-ready");
		$container.find(".play").addClass("hide");
	} else {
		if (buildingInfo.ready) {
			$container.removeClass("building-not-deployed");
 		$container.addClass("building-ready");
 		if (playerInfo.bank >= buildingInfo.cost) {
 			$container.find(".play").removeClass("hide");	
 		} else {
 			$container.find(".play").addClass("hide");	
 		}
 		
 	} else {
 		$container.addClass("building-not-deployed");	
 	}
	}
}

function playerLoss(name) {
	$(".end-game").removeClass("hide").text(name + " has been defeated");
	$("body").one("click", function(){
		$(".end-game").addClass("hide");
		window.location.href = "index.php"
	})
}

function sendAction (params, callback, debug) {
	params.player = PLAYER_NAME;
	$.ajax({
	  type: "POST",
	  url: CONTROLLER_URL,
	  data: params,
	  dataType: "json",
	  cache: false
	})
	.done(function(response){
		if (debug == true) {
			console.log("response:");
			console.log(response);
		}
		if (response.result == "success"){
			callback(response);
		} else {
			console.error(params.action + "return with error");
			if (response.message) {
				console.error(response.message);
			}
		}
	})
	.error(function(xhr, status, error){
		console.error("error on action " + params.action);
		console.error(error.stack);
	});
}

function removeStringFromArray (arr, string) {
	var index = arr.indexOf(string);
	while (index !== -1) {
		arr.splice(index, 1);
		index = arr.indexOf(string);
	}
}