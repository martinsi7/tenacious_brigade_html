var buildingCards = [];
var mechCards = [];
var unitCards = [];

var selectedBuildingCards = [];
var selectedMechCards = [];
var selectedUnitCards = [];

var hasBuildingError = true;
var hasMechError = true;
var hasTotalError = true;

$(document).ready(function(){
	getCardList({isAjax: true});

	$(this).on("click", ".card", function(){
		$(this).toggleClass("highlighted");
		zoomCardRightPane($(this));
		addOrRemoveCard($(this));
		updateErrors();
	})

	$("#left-pane").height($(window).height());
	$("#left-pane").width($(window).width() * .7);
	$("#show-all").click(function(){
		getCardList({isAjax: true});
	});
	$("#show-old-guard").click(function(){
		getCardList({isAjax: true, faction: "Old Guard"});
	});
	$("#show-anarchist-youth").click(function(){
		getCardList({isAjax: true, faction: "Anarchist Youth"});
	});
	$("#show-professionals").click(function(){
		getCardList({isAjax: true, faction: "Professionals"});
	});

	$("#submit-deck").click(submitDeck)
});

function getCardList(params) {
	$(".card").remove();
	buildingCards = [];
	mechCards = [];
	unitCards = [];
	$.ajax({
	  type: "GET",
	  url: "build.php",
	  data: params,
	  dataType: "json",
	  cache: false
	})
	.done(function(response){
		setCards(response);
		drawCards();
	});
}

function setCards(response){
	for (var i in response[0]) {
		var dBCard = response[0][i];
		var cardInfo = convertDBCardToCardInfo(dBCard);
		allCards[cardInfo.id] = cardInfo
		buildingCards.push(cardInfo);
	}
	for (var i in response[1]) {
		var dBCard = response[1][i];
		var cardInfo = convertDBCardToCardInfo(dBCard);
		allCards[cardInfo.id] = cardInfo
		mechCards.push(cardInfo);
	}
	for (var i in response[2]) {
		var dBCard = response[2][i];
		var cardInfo = convertDBCardToCardInfo(dBCard);
		allCards[cardInfo.id] = cardInfo
		unitCards.push(cardInfo);
	}
}

function convertDBCardToCardInfo(dBCard) {
	var temp = {};
	temp.gameID = dBCard.card_id;
	temp.name = dBCard.card_name;
	temp.description = dBCard.card_description;
	temp.attack = dBCard.attack;
	temp.health = dBCard.health;
	temp.faction = dBCard.faction;
	temp.cost = dBCard.cost;
	temp.cardType = dBCard.card_type;
	temp.unitType = dBCard.unitType;
	return new CardInfo(temp);
}

function drawCards() {
	for (var i in buildingCards) {
		cardInfo = buildingCards[i];
		$card = instantiateCard(cardInfo);
		udpateCardsAttributes($card, cardInfo);
		if (isCardInArr(cardInfo.id, selectedBuildingCards)) {
			$card.addClass("highlighted");
		}
		$(".building-cards .card-placement").append($card);
	}
	for (var i in mechCards) {
		cardInfo = mechCards[i];
		$card = instantiateCard(cardInfo);
		udpateCardsAttributes($card, cardInfo);
		if (isCardInArr(cardInfo.id, selectedMechCards)) {
			$card.addClass("highlighted");
		}
		$(".mech-cards .card-placement").append($card);
	}
	for (var i in unitCards) {
		cardInfo = unitCards[i];
		$card = instantiateCard(cardInfo);
		udpateCardsAttributes($card, cardInfo);
		if (isCardInArr(cardInfo.id, selectedUnitCards)) {
			$card.addClass("highlighted");
		}
		$(".unit-cards .card-placement").append($card);
	}
}

function zoomCardRightPane($card) {
	var clone = $card.clone();
	clone.attr("id", "");
	clone.find("div").removeClass("hide");
	$(".right-card-display").html(clone);
}

function addOrRemoveCard($card) {
	var id = $card.attr("id")
	var cardRef = allCards[id];
	console.log(cardRef);
	if ($card.hasClass("highlighted")) {
		console.log("need to add" + $card.attr("id"));
		if (cardRef.cardType == "building") {
			selectedBuildingCards.push(cardRef);
		} else if (cardRef.cardType == "mech") {
			selectedMechCards.push(cardRef);
		} else if (cardRef.cardType == "unit") {
			selectedUnitCards.push(cardRef);
		}
	} else {
		console.log("need to remove" + $card.attr("id"));
		if (cardRef.cardType == "building") {
			removeCard(id, selectedBuildingCards);
		} else if (cardRef.cardType == "mech") {
			removeCard(id, selectedMechCards);
		} else if (cardRef.cardType == "unit") {
			removeCard(id, selectedUnitCards);
		}
	}
}

function removeCard(id, arr) {
	var idx = -1;
	for (var i in arr) {
		var cardInfo = arr[i];
		if (cardInfo.id == id) {
			idx = i;
			break;
		}
	}
	if (idx > -1) {
		arr.splice(idx, 1);
	}
}

function isCardInArr(id, arr) {
	var idx = -1;
	for (var i in arr) {
		var cardInfo = arr[i];
		if (cardInfo.id == id) {
			idx = i;
			break;
		}
	}
	return idx > -1
}

function updateErrors() {
	var numBuildings = selectedBuildingCards.length;
	$("#total-building .count").text(numBuildings);
	if (numBuildings == 1) {
		$("#total-building .count").removeClass("error");
		hasBuildingError = false;
	} else {
		$("#total-building .count").addClass("error");
		hasBuildingError = true;
	}

	var numMech = selectedMechCards.length;
	$("#total-mech .count").text(numMech);
	if (numMech >= 3 && numMech <= 6) {
		$("#total-mech .count").removeClass("error");
		hasMechError = false;
	} else {
		$("#total-mech .count").addClass("error");
		hasMechError = true;
	}	

	var numTotal = numBuildings + numMech + selectedUnitCards.length;
	$("#total-cards .count").text(numTotal);
	if (numTotal == 14) {
		$("#total-cards .count").removeClass("error");
		hasTotalError = false;
	} else {
		$("#total-cards .count").addClass("error");
		hasTotalError = true;
	}	

	if (hasTotalError || hasMechError || hasBuildingError) {
		$("#submit-deck").addClass("hide");
	} else {
		$("#submit-deck").removeClass("hide");
	}
}

var submitDeck = function() {
	var cardIDs = [];
	var name = $("#deck-name").val();
	if (name == "") {
		$("#deck-name").addClass("error")
		$("#deck-name").focus();
		return;
	} else {
		$("#deck-name").removeClass("error")
	}
	for (var i in selectedBuildingCards) {
		cardIDs.push(selectedBuildingCards[i].id);
	}
	for (var i in selectedMechCards) {
		cardIDs.push(selectedMechCards[i].id);
	}
	for (var i in selectedUnitCards) {
		cardIDs.push(selectedUnitCards[i].id);
	}

	$.ajax({
	  type: "POST",
	  url: "build.php",
	  data: {name: name, cards: cardIDs},
	  dataType: "json",
	  cache: false
	})
	.done(function(response){
		location.href = "index.php";
	})
	.fail(function(a, b) {
		console.error(a);
		console.error(b);
	});
}