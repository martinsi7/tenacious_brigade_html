

$(document).ready(function(){
	$(".begin-game").click(function(){
		begin_game($(this).attr("id").split("_")[1]);
	});
	$(this).on("click", ".deck-choice", function(){
		selectDeck($(this).attr("id"));
	})

	$(".end-turn-button").click(function(){
		endTurn();
	});
	$(".attack-button").click(function() {
		$(".attack-turn").addClass("hide");
		switchPhase();
	})

	$("#zoom-card-display").on("click", ".ability", function() {
		if (curPhase == gamePhases[1] || curPhase == gamePhases[2]) {
			return;
		}
		card = allCards[zoomedCardID];
		if (card.cardType == "mech") {
			var cost = $(this).text().split(":")[0]
			activateAbility(zoomedCardID, $(this).index(), cost);
		} else if (card.isInPlay && card.cooldown <= 0 && card.isPrepTime == false) {
		//if (card.isInPlay && card.isPrepTime == false) {
			var cost = $(this).text().split(":")[0]
			activateAbility(zoomedCardID, $(this).index(), cost);
		}
	});

	$(this).on("click", ".debt", function(){
		if (curPhase == gamePhases[1] || curPhase == gamePhases[2]) {
			return;
		}
		var playerPrefix = activePlayer.selectorPrefix;
		if (playerPrefix.indexOf($(this).closest(".player-card").attr("id")) !== -1) {
			payDebt();
		}
	})

	$(".cancel-action-button").click(cancelAction);

	$("#zoom-card-display").on("click", ".change-lane", function() {
		changeLane();
	});

	$(".building-card .play").on("click", function(event) {
		if (curPhase == gamePhases[1] || curPhase == gamePhases[2]) {
			return;
		}
		activateBuilding($(event.target).parent().attr("id"));
	})
});

function begin_game(playerName){
	PLAYER_NAME = playerName;
	$(".begin-game").addClass("hide");

	$.ajax({
	  type: "GET",
	  url: "play.php",
	  data: {action: "getCards"},
	  dataType: "json",
	  cache: false
	})
	.done(function(cards){
		params = {
			action: "startGame",
			cards: cards
		};
		sendAction(params, function(response) {
			if (response.isWaiting) {
				$(".end-game").removeClass("hide");
				$(".end-game").text("waiting for other player");
				checkGameFull();
			} else {
				$(".deck-choice").remove();
				$("#game-board").removeClass("hide");
				allCards = {};
				updateCards(response.data.cards);
				updatePlayers(response.data.players);
				activePlayer = allPlayers["one"];
				otherPlayer = allPlayers["two"];
				zoomPlayer("one");
				doLogic();
			}
		}, false);
	});

	
}

var deckOne = "";

function selectDeck(name) {
	var params = {}
	if (PLAYER_NAME == "both") {
		if (deckOne == "") {
			deckOne = name.split("_").join(" ");
			return;
		} else {
			params = {
				action: "beginGame",
				deckNameOne: deckOne,
				deckNameTwo: name.split("_").join(" ")
			};
		}
	} else {
		params = {
			action: "beginGame",
			deckName: name.split("_").join(" ")
		};	
	}

}

function checkGameFull(){
	var params = {
		action: "checkGameFull"
	};
	sendAction(params, function(response) {
		if (response.ready) {
			$(".end-game").addClass("hide");
			$("#game-board").removeClass("hide");
			allCards = {};
			updateCards(response.data.cards);
			updatePlayers(response.data.players);
			activePlayer = allPlayers["one"];
			otherPlayer = allPlayers["two"];
			zoomPlayer("one");
			doLogic();
		} else {
			window.setTimeout(function(){
				checkGameFull();
			}, 1000)
		}
	});
}

function isCurPlayer() {
	//if (curPhase == gamePhases[2]) {
	//	return (activePlayer.name != PLAYER_NAME);
	//}
	if (PLAYER_NAME == "both") {
		return true;
	}
	return (activePlayer.name == PLAYER_NAME);
}

function checkForChanges() {
	var params = {
		action: "checkForChanges"
	};
	sendAction(params, function(response) {
		if (response.hasChanges) {
			var isShowingResults = false;
			updateCards(response.data.cards);
			updatePlayers(response.data.players);
			updateValidCombats(response);
			if ("combatResults" in response) {
				isShowingResults = true;
			}
			if ("phase" in response.data) {
				updatePhase(response);
			}
			if (!isCurPlayer()) {
				window.setTimeout(function(){
					checkForChanges();
				}, 1000);
			} else if (!isShowingResults) {
				doLogic();
			} else {
				showCombatResults(response.combatResults);
			}
		} else {
			window.setTimeout(function(){
				checkForChanges();
			}, 1000);
		}
	})
}

function showCombatResults(results) {
	var hasDefenders = false;
	for (var i in results) {
		var combat = results[i];
		if (!("defenders" in combat)) {
			continue;
		}
		if (combat.defenders.length > 0) {
			hasDefenders = true;
			break;
		}
	}
	if (!hasDefenders) {
		doLogic();
		return;
	}
	for (var i in results) {
		var combat = results[i];
		var attackerID = combat.attacker;
		var cardObj = $("#" + attackerID);
		cardObj.addClass("combat-card-" + i);
		if (!("defenders" in combat)) {
			continue;
		}
		for (var j in combat.defenders) {
			var defenderID = combat.defenders[j];
			var defenderCard = $("#" + defenderID);
			defenderCard.addClass("combat-card-" + i);
		}
	}
	$(".action-button").each(function(){
		$(this).parent().addClass("disabled");
	});
	$(".showing-combat-results").removeClass("hide");
	$("body").scrollTop($("#player-two-player").offset().top);
	window.setTimeout(function() {
		unShowCombatResults();
	}, 3000)
}

function unShowCombatResults(){
	$(".card").each(function(){
		var combatClass = getCombatCardClass($(this));
		if (combatClass !== "") {
			$(this).removeClass(combatClass);
		}
		if (playerCopy != null) {
			playerCopy.remove();
		}
	})
	$(".clone").remove();
	$(".action-button").each(function(){
		$(this).parent().removeClass("disabled");
	});
	$(".showing-combat-results").addClass("hide");
	doLogic();
}

activePlayer = {};
otherPlayer = {};
attackingCards = [];
combats = [];
targets = [];
validAttackers = [];
canPlayerAttack = false;
validCombats = [];
playerLane = "";
numAllowedTargets = 0;

gamePhases = [
	"Main Phase One",
	"Attack Phase",
	"Defense Phase",
	"Main Phase Two"
];

curPhaseIndex = 0;
curPhase = gamePhases[curPhaseIndex];
curTurn = 1;

function defaultActionCallback(params) {
	sendAction(params, function(response){
		updateCards(response.data.cards);
		updatePlayers(response.data.players);
		doLogic();
	}, false);
}

function doLogic(){
	if (!isCurPlayer()) {
		checkForChanges();
		$(".action-button").addClass("disabled");
		return;
	} else {
		$(".action-button").removeClass("disabled");
	}
	$(".active").removeClass("active");
	$(".active-multiple").removeClass("active-multiple");
	highlightedCard = null;
	if (curPhase == gamePhases[0] || curPhase == gamePhases[3]) {
		doMainPhase();
	} else if (curPhase == gamePhases[1]) {
		doAttackPhase();
	} else if (curPhase == gamePhases[2]){
		doDefensePhase();
	}
}

function switchPhase(){
	sendAction({"action": "nextPhase"}, function(response){
		updateCards(response.data.cards);
		updatePlayers(response.data.players);
		updateValidCombats(response);
		updatePhase(response);
		doLogic();
	}, true)
}

function endTurn() {
	sendAction({"action": "endTurn"}, function(response) {
		updateCards(response.data.cards);
		updatePlayers(response.data.players);
		updatePhase(response);
		doLogic();
	})
}

function updatePhase(response) {
	curPhase = response.data.phase;
	curPhaseIndex = gamePhases.indexOf(curPhase);
	//$(".phase").text(curPhase);
	if ("turn" in response.data) {
		curTurn = response.data.turn;
		$(".current-turn").text(curTurn);
	}
	if (activePlayer != allPlayers[response.data.activePlayer]) {
		zoomPlayer(response.data.activePlayer);
	}
	activePlayer = allPlayers[response.data.activePlayer];
	if (activePlayer.name == "one"){
		otherPlayer = allPlayers["two"];
	} else {
		otherPlayer = allPlayers["one"];
	}
	$(".player-name").text(activePlayer.name);
	doActiveCardFunc = doNothing;
}

function updateValidCombats(response){
	if ("validAttackers" in response) {
		validAttackers = response["validAttackers"];
		canPlayerAttack = response["canPlayerAttack"];
	} /*else {
		validAttackers = []
		canPlayerAttack = false;
	}*/
	if ("validCombats" in response) {
		validCombats = response["validCombats"];
	}/* else {
		validCombats = [];
	}*/
}

function zoomPlayer(playerName) {
	if (playerName == "one") {
		$("body").scrollTop($("body").height() * 2);
	} else {
		$("body").scrollTop(0);
	}
}

function doMainPhase() {
	attackingCards = [];
	combats = [];
	$(".clone").remove();
	$(".highlighted").removeClass("highlighted");
	$(".confirm-attack").addClass("hide");
	$(".end-turn").removeClass("hide");
	if (curPhase == gamePhases[0] && activePlayer.canAttack) {
		$(".attack-turn").removeClass("hide");
	} else {
		$(".attack-turn").addClass("hide");
	}
	var handAndClinic = $(activePlayer.selectorPrefix + "hand, " + activePlayer.selectorPrefix + "clinic");
	var bank = activePlayer.bank;
	handAndClinic.find(".card").each(function(){
		var cardCost = parseInt($(this).find(".cost").text().substring(1));
		if (cardCost <= bank) {
			$(this).addClass("active");
		} else {
			$(this).removeClass("active");
		}
		doActiveCardFunc = placeCardFunc;
	});
}

function doAttackPhase() {
	$(".end-turn").addClass("hide");
	$(".confirm-attack").removeClass("hide");
	for (i in validAttackers) {
		unitID = validAttackers[i];
		$("#" + unitID).addClass("active-multiple");
	}
	if (canPlayerAttack) {
		if (activePlayer.name == "one") {
			$("#player-one").addClass("active-multiple");
		} else {
			$("#player-two").addClass("active-multiple");
		}
	}
	doActiveMultipleFunc = selectAttackerFunc;
	undoActiveMultipleFunc = unselectAttackerFunc;
	doConfirmCombatFunc = sendAttackerInfo;
}

function doDefensePhase() {
	$(".attack-turn").addClass("hide");
	$(".end-turn").addClass("hide");
	$(".confirm-attack").removeClass("hide");
	$(".highlighted").removeClass("highlighted");
	var i = 0;
	for (var attackerID in validCombats){
		//var attackerID = attackingCards[i];
		if (attackerID.toLowerCase().indexOf("player") !== -1){
			createPlayerClone(attackerID);
		}
		var cardObj = $("#" + attackerID);
		cardObj.addClass("active");
		cardObj.addClass("combat-card-" + i);
		combats.push({attacker: attackerID, defenders: []})
		i++;
	}
	doActiveCardFunc = selectAttackerToDefendFunc;
	doActiveMultipleFunc = selectDefenderFunc;
	undoActiveMultipleFunc = unselectDefenderFunc;
	doConfirmCombatFunc = sendCombatInfo;
}

function createPlayerClone(id) {
	var lane = id.split("_")[1];
	var prefix = otherPlayer.selectorPrefix;
	var $card = $(prefix.substring(0, prefix.length - 1));
	var $clone = $card.clone();
	$clone.attr("id", id);
	$clone.addClass("clone");
	$(prefix + lane).append($clone);
}

var sendAttackerInfo = function(){
	doConfirmCombatFunc = doNothing;
	params = {
		action: "setAttackers",
		attackers: attackingCards
	};
	sendAction(params, function(response){
		if (!("data" in response)) {
			doLogic();
			return;
		}
		updateCards(response.data.cards);
		updatePlayers(response.data.players);
		updateValidCombats(response);
		updatePhase(response);
		doLogic();
	}, false);
}

var sendCombatInfo = function(){
	doConfirmCombatFunc = doNothing;
	var params = {
		action: "combat",
		combats: combats
	};
	sendAction(params, function(response){
		if (response.data != null){
			updateCards(response.data.cards);
			updatePlayers(response.data.players);
		}
		$(".end-phase").removeClass("hide");
		$(".confirm-attack").addClass("hide");
		$(".highlighted").removeClass("highlighted");
		attackingCards = [];
		combats = [];
		$(".card").each(function(){
			var combatClass = getCombatCardClass($(this));
			if (combatClass !== "") {
				$(this).removeClass(combatClass);
			}
			if (playerCopy != null) {
				playerCopy.remove();
			}
		})
		$(".clone").remove();
		if (!("data" in response)) {
			doLogic();
			return;
		}
		updateCards(response.data.cards);
		updatePlayers(response.data.players);
		updateValidCombats(response);
		updatePhase(response);
		doLogic();
	}, true)
}

var activateAbility = function(cardID, index, cost) {
	cost = cost.substring(1);
	if (Number(cost) > activePlayer.bank) {
		return;
	}
	if (curPhase == gamePhases[1] || curPhase == gamePhases[2]) {
		return;
	}
	var params = {
		action: "activateAbility",
		cardID: cardID,
		index: index
	};
	//var confirmation = confirm("Do you want to spend " + cost + " on this ability?");
	//if (!confirmation) {
	//	return;
	//}
	sendAction(params, function(response) {
		$("#zoom-card-display").addClass("hide")
		$("#game-board").fadeTo("fast", 1);
		updateCards(response.data.cards);
		updatePlayers(response.data.players);
		$(".active").removeClass("active");
		$(".highlighted").removeClass("highlighted");
		if (response.targetInfo != null)  {
			for (var i in response.targetInfo.validTargets) {
				var targetID = response.targetInfo.validTargets[i];
				$("#" + targetID).addClass("valid-target active-multiple");
			}
			$(".end-turn").addClass("disabled");
			$(".attack-turn").addClass("disabled");
			$(".cancel-action").removeClass("hide");
			doActiveMultipleFunc = selectTargetsFunc;
			undoActiveMultipleFunc = unselectTargetsFunc;
			doActiveCardFunc = null;
			numAllowedTargets = response.targetInfo.numTargets;
		}
	}, true)
}

var cancelAction = function() {
	$(".end-turn").removeClass("disabled");
	$(".attack-turn").removeClass("disabled");
	$(".valid-target").removeClass("valid-target");
	$("active-multiple").removeClass("active-multiple");
	doActiveMultipleFunc = doNothing;
	undoActiveMultipleFunc = doNothing;
	$(".highlighted").removeClass("highlighted");
	$(".cancel-action").addClass("hide");
	targets = [];
	doLogic();
}

var changeLane = function() {
	if (curPhase == gamePhases[1] || curPhase == gamePhases[2]) {
		return;
	}
	if (!isCurPlayer()) {
		return;
	}
	var card = allCards[zoomedCardID];
	var lane = getLane(card.zone);
	var otherLaneObjs = getOtherLaneObjs(lane, activePlayer.selectorPrefix);
	otherLaneObjs.addClass("placement-zone");
	unZoom();
	window.setTimeout(function() {
		$("body").one("click", function(event) {
			$target = $(event.target);
			if ($target.hasClass("placement-zone")) {
				sendChangeLane(card, getLane($target.attr("id")));
			}
			$(".placement-zone").removeClass("placement-zone")
		})
	}, 300)
	
}

var activateBuilding = function(id) {
	if (activePlayer.building.id == id) {
		var buildingInfo = activePlayer.building;
		if (buildingInfo.cost <= activePlayer.bank && confirm(
			"Do you want to pay " + buildingInfo.cost + " to activate this building?")) {
			var params = {
				action: "activateBuilding"
			}
			sendAction(params, function(response) {
				updateCards(response.data.cards);
				updatePlayers(response.data.players);
			}, true)
		}
	}
}

var sendChangeLane = function(card, newLane){
	var params = {
		action: "changeLane",
		cardID: card.id,
		newLane: newLane
	};
	sendAction(params, function(response) {
		updateCards(response.data.cards);
		updatePlayers(response.data.players);
	}, true)
}

var getOtherLaneObjs = function(lane, playerPrefix) {
	if (lane == "left") {
		return $(playerPrefix + "center");
	} else if (lane == "center") {
		return $(playerPrefix + "left, " + playerPrefix + "right");
	} else {
		return $(playerPrefix + "center");
	}
}

var payDebt = function(){
	if (confirm("Would you like to pay your debt?")) {
		var params = {
			action: "payDebt"
		};
		sendAction(params, function(response){
			updateCards(response.data.cards);
			updatePlayers(response.data.players);
		}, true);
	}
}

var sendTargets = function() {
	//TODO: handle player targetting
	var params = {
		action: "selectTargets",
		targetInfo: {
			canTargetOtherPlayer: false,
			canTargetSelfPlayer: false,
			validTargets: targets
		}
	};
	sendAction(params, function(response) {
		updateCards(response.data.cards);
		updatePlayers(response.data.players);
		cancelAction()
	}, true)
}

var selectDefenderFunc = function(cardObj){
	if (highlightedCard == null){
		cardObj.removeClass("highlighted");
		return;
	}
	for (var i in combats) {
		var combat = combats[i];
		if (combat.attacker == highlightedCard.attr("id")){
			combat.defenders.push(cardObj.attr("id"));
			var combatClass = getCombatCardClass(highlightedCard);
			cardObj.addClass(combatClass);
			return;
		}
	}
}

var unselectDefenderFunc = function(cardObj){
	for (var i in combats) {
		var defenders = combats[i].defenders;
		removeStringFromArray(defenders, cardObj.attr("id"));
		var combatClass = getCombatCardClass(cardObj);
		cardObj.removeClass(combatClass);
	}
}

var selectAttackerToDefendFunc = function(cardObj){
	$(".active-multiple").removeClass("active-multiple");
	var attackerID = cardObj.attr("id");
	var attacker = allCards[attackerID];
	// used for mech clone
	/*if (attacker == null) {
		var zone = cardObj.parent().attr("id");
		attacker = {zone: zone};
	}
	var placement = $(otherPlayer.selectorPrefix + "placement");
	placement.find(".card").each(function(){
		var info = allCards[$(this).attr("id")];
		if (info.cooldown == 0 && isOppositeLanes(attacker, info)) {
			$(this).addClass("active-multiple");
		}
	})*/
	var defenders = validCombats[attackerID];
	for (i in defenders) {
		defenderID = defenders[i];
		$("#" + defenderID).addClass("active-multiple");
	}
}

var selectTargetsFunc = function(cardObj) {
	var id = cardObj.attr("id");
	targets.push(cardObj.attr("id"));
	if (targets.length > numAllowedTargets) {
		throw ("too many targets");
	}
	if (targets.length == numAllowedTargets) {
		sendTargets();
	}
}

var unselectTargetsFunc = function(cardObj) {
	cardObj.removeClass("highlighted");
	removeStringFromArray(targets, cardObj.attr("id"));
	return;
}

function getCombatCardClass(cardObj) {
	var classes = cardObj.attr("class").split(" ");
	for (var i in classes) {
		var combatClass = classes[i];
		if (combatClass.indexOf("combat-card") !== -1) {
			return combatClass;
		}
	}
	return "";
}

function isOppositeLanes(card1, card2){
	var lane1 = getLane(card1.zone);
	var lane2 = getLane(card2.zone);
	if (lane1 == lane2) {
		return true;
	} else {
		return false;
	}
}

function getLane(zone){
	if (zone.indexOf("left") !== -1){
		return "left";
	} else if (zone.indexOf("right") !== -1){
		return "right";
	} else if (zone.indexOf("center") !== -1){
		return "center";
	}
	return "";
}

playerCopy = null;

var selectAttackerFunc = function(cardObj) {
	if (cardObj.attr("id").indexOf("player") !== -1) {
		var cardRow = $(activePlayer.selectorPrefix + "placement");
		cardRow.addClass("placement-zone");
		$("body").one("click", function(event){
			cardRow.removeClass("placement-zone");
			$target = $(event.target)
			if (($target).hasClass("lane")) {
				playerCopy = cardObj.clone();
				var lane = getLane($target.attr("id"));
				playerCopy.attr("id", cardObj.attr("id") + "-clone_" + lane)
				playerCopy.addClass("clone");
				$target.append(playerCopy);
				attackingCards.push(playerCopy.attr("id"));
			} else {
				cardObj.removeClass("highlighted");
			}
		});
	} else {
		attackingCards.push(cardObj.attr("id"));
	}
}

var unselectAttackerFunc = function(cardObj){
	removeStringFromArray(attackingCards, cardObj.attr("id"));
	cardObj.removeClass("highlighted")
	if (cardObj.attr("id") === playerCopy.attr("id")) {
		playerCopy.remove();
	}
}

var placeCardFunc = function(){
	var cardInfo = allCards[highlightedCard.attr("id")];
	if (cardInfo.cardType == "mech") {
		var cardRow = $(activePlayer.selectorPrefix + "mech");
		cardRow.addClass("placement-zone");
		$("body").one("click", function(event){
			cardRow.removeClass("placement-zone");
			if (($(event.target)).is(cardRow)) {
				params = {
					"action": "playCard",
					"cardID": highlightedCard.attr("id"),
					"zone": $(event.target).attr("id")
				}
				defaultActionCallback(params);
			}
		});
	} else {
		var cardRow = $(activePlayer.selectorPrefix + "placement");
		cardRow.addClass("placement-zone");
		$("body").one("click", function(event){
			cardRow.removeClass("placement-zone");
			if (($(event.target)).hasClass("lane")) {
				params = {
					"action": "playCard",
					"cardID": highlightedCard.attr("id"),
					"zone": $(event.target).attr("id")
				}
				defaultActionCallback(params);
			}
		});
	}
}