<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
session_start();

require_once("game_client.php");

if (!isset($_POST["action"])) {
	echo json_encode(array("success" => false));
	exit();
}


$result = send_request($_POST);
echo json_encode($result);


?>