<?php
if ($argc != 2) {
	echo "usage: convert.php [filename]\n";
	exit();
}
echo $argc;
$input_file = $argv[1];
$file_name = date("Y-m-d-H-i-s-") . explode(".", $input_file)[0] . ".sql";
echo "creating file: " . $file_name;
$file_contents = "INSERT INTO Card\n";
$file_contents .= "(card_name,cost,attack,health,card_description,faction,card_type,unit_type)\nVALUES";
$row = 1;
if (($handle = fopen($input_file, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
        //echo "$num fields in line $row\n";
        $row++;
        if ($row == 2) {
        	continue;
        }
        $file_contents .= "\n(";
        for ($c=0; $c < $num; $c++) {
        	$item = $data[$c];
        	if ($item == "" && ($c == 7 || $c == 3 || $c == 2)) {
        		$item = "null";
        	} else if ($c != 1 && $c != 2 && $c != 3) {
        		$file_contents .= "'";
        	} else if ($item == "") {
        		$item == "null";
        	}
        	
            $file_contents .= $item;
            if ($c != 1 && $c != 2 && $c != 3 && $item != "null") {
        		$file_contents .= "'";
        	}
            if (!($c == $num - 1)) {
            	$file_contents .= ",";
            }
        }
        $file_contents .= "),";
    }
    //echo $file_contents[strlen($file_contents) - 1];
    $file_contents = rtrim($file_contents, ",");
    $file_contents .= ";";
    fclose($handle);
    file_put_contents($file_name, $file_contents);
}
?>