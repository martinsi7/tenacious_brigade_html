# Standalone tables

CREATE TABLE Player (
	player_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	player_name VARCHAR(30) NOT NULL,
	email VARCHAR(50) NOT NULL,
	is_admin TINYINT default 0,
	UNIQUE(email)
);

CREATE TABLE Card (
	card_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	card_name VARCHAR(30) NOT NULL,
	card_description VARCHAR(150),
	image_file VARCHAR(30),
	attack SMALLINT,
	health SMALLINT,
	faction VARCHAR(20),
	card_type VARCHAR(20),
	unit_type VARCHAR(20),
	cost SMALLINT,
	rarity VARCHAR(20) default 'common'
);

# Dependent tables

CREATE TABLE Deck (
	deck_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	player_id INT(6) UNSIGNED,
	deck_name VARCHAR(30) NOT NULL,
	FOREIGN KEY (player_id) REFERENCES Player(player_id)
		ON DELETE CASCADE,
	UNIQUE KEY player_id_deck_name (player_id, deck_name)
);

CREATE TABLE Collection (
	player_id INT(6) UNSIGNED,
	card_id INT(6) UNSIGNED,
	deck_id INT(6) UNSIGNED,
	FOREIGN KEY (player_id) REFERENCES Player(player_id),
	FOREIGN KEY (deck_id) REFERENCES Deck(deck_id)
);

CREATE TABLE DeckList (
	deck_list_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	deck_id INT(6) UNSIGNED,
	card_id INT(6) UNSIGNED,
	FOREIGN KEY (deck_id) REFERENCES Deck(deck_id)
		ON DELETE CASCADE,
	FOREIGN KEY (card_id) REFERENCES Card(card_id)
		ON DELETE CASCADE
);

