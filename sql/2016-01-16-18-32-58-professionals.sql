INSERT INTO Card
(card_name,cost,attack,health,card_description,faction,card_type,unit_type)
VALUES
('Disguised Civilian',10,2,1,'$30, cooldown: infiltrate','Professionals','unit','specialist'),
('Veteran Tactician',30,1,3,'$0, cooldown: Give target fighter in this lane snipe 2 until end of turn','Professionals','unit','specialist'),
('Patient Invader',50,2,2,'$0, cooldown: Infiltrate. Gain +1/+1','Professionals','unit','specialist'),
('Glass Shard Slinger',30,3,2,'Shambling.
$0, cooldown: Infiltrate','Professionals','unit','specialist'),
('Covert Companion',20,0,1,'$10: target unit in this lane gains infiltrate until end of turn
$10, cooldown: target unit in this lane with lane with infiltrate recovers 1 cooldown','Professionals','unit','specialist'),
('Small Arms Dealer',30,2,2,'At the beginning of your turn, the fighter with the lowest attack gains +2/+1 and tenacity until the beginning of your next turn','Professionals','unit','specialist'),
('Inspiring Agent',40,3,2,'$20: 2 target specialists in this lane gain +1/+0 and become fighters until the beginning of your next turn','Professionals','unit','specialist'),
('Prolific Sniper',70,2,4,'Cooldown: 2 target units in the opposing lane gain Cooldown 1','Professionals','unit','specialist'),
('Camoflaged Spotter',20,1,2,'Cooldown: Give target unit in this lane +0/+1 and Snipe 2 until the beginning of your next turn ','Professionals','unit','specialist'),
('Rifle Cart',40,0,4,'Deployment: Give all specialists in this lane snipe 2','Professionals','unit','specialist'),
('Roving Infiltrator',50,1,3,'Mobility
Cooldown: Move this unit to the center lane and Infiltrate','Professionals','unit','specialist'),
('Fresh Inductee',40,1,2,'Snipe 2','Professionals','unit','fighter'),
('Steadfast Killer',50,1,5,'Snipe 4','Professionals','unit','fighter'),
('Trained Brute',40,5,5,'Contract 20','Professionals','unit','fighter'),
('Methodical Striker',70,7,5,'Contract 10
Deployment: If it is the only friendly fighter in this lane, it gains Mobility','Professionals','unit','fighter'),
('Reckless Operative',30,4,4,'Snipe 2
If it deals combat damage to a unit, put Reckless Operative in the clinic at the end of combat','Professionals','unit','fighter'),
('Long Scope Merc',20,2,3,'Contract 20
Reach','Professionals','unit','fighter'),
('Cautious Killer',30,1,4,'At the beginning of your turn, gain Snipe 2 until end of turn','Professionals','unit','fighter'),
('Mini Kill Bot',10,2,1,'Tenacity','Professionals','unit','fighter'),
('Invading Rifleman',10,2,2,'Aggressive
Snipe 1','Professionals','unit','fighter'),
('Local Defense Agent',20,2,4,'Defensive
Snipe 1','Professionals','unit','fighter'),
('Laser Guide',40,1,null,'At the beginning of your turn, the friendly Professional specialist with the highest attack gains Snipe +2 until the beginning of your next turn','Professionals','mech',null),
('Targeted Stealth Field',30,0,null,'At the beginning of your turn, two friendly Professional fighters with the highest attack gain Cooldown: Infiltrate until the end of your turn.','Professionals','mech',null);