# lines starting with # are comments and are not interpreted 
# you can add these wherever you feel like it, including nowhere

#table description

#test

# +------------------+-----------------+------+-----+---------+----------------+
# | Field            | Type            | Null | Key | Default | Extra          |
# +------------------+-----------------+------+-----+---------+----------------+
# | card_id          | int(6) unsigned | NO   | PRI | NULL    | auto_increment |
# | card_name        | varchar(30)     | NO   |     | NULL    |                |
# | card_description | varchar(150)    | YES  |     | NULL    |                |
# | image_file       | varchar(30)     | YES  |     | NULL    |                |
# | attack           | smallint(6)     | YES  |     | NULL    |                |
# | health           | smallint(6)     | YES  |     | NULL    |                |
# | faction          | varchar(20)     | YES  |     | NULL    |                |
# | card_type        | varchar(20)     | YES  |     | NULL    |                |
# | unit_type        | varchar(20)     | YES  |     | NULL    |                |
# +------------------+-----------------+------+-----+---------+----------------+


INSERT INTO Card 
	(card_name,
     cost,
     attack,
     health,
	 card_description,
	 faction,
	 card_type,
	 unit_type,
     image_file)
VALUES
# the part you will be adding to looks like everything below
# each card is contained by parens () and comma separated
# each of the cards attributes are also comma separated
# commas never go after the last element
# varchar fields must be bounded by single quotes ''
# everything is case sensitive
# Have fun :)



# examples below are unit/fighter, unit/specialist, mech, and building
# formats should be exactly the same
# order makes no difference
(
    'Reinforced Turret',
    20,
    3,
    4,
    'Defensive',
    'Old Guard',
    'unit',
    'fighter',
    ''
),
(
    'Stimulant Vendor',
    30,
    0,
    4,
    '10: Target unit in this lane gets Tenacity until the beginning of your next 
    turn',
    'Old Guard',
    'unit',
    'specialist',
    ''
),
(
    'Stubborn Veteran',
    30,
    1,
    4,
    'Defensive, Regeneration, 10: +2/+0 until the beginning of your next turn',
    'Old Guard',
    'unit',
    'fighter',
    ''
),
(
    'Weathered Mercenary',
    40,
    5,
    3,
    'Contract: 10, Tenacity',
    'Old Guard',
    'unit',
    'fighter',
    ''
),
(
    'Turreted Barricade',
    40,
    1,
    5,
    'Defensive, Reach',
    'Old Guard',
    'unit',
    'fighter',
    ''
),
(
    'Heavy Arms Dealer',
    40,
    1,
    3,
    'At the beginning of your turn, the fighter with the highest health in this
    lane gets +3/+3 until the beginning of your next turn',
    'Old Guard',
    'unit',
    'specialist',
    ''
),
(
    'Makeshift Tank',
    50,
    5,
    8,
    'Defensive, Shambling, When this unit takes combat damage, it loses Defensive',
    'Old Guard',
    'unit',
    'fighter',
    ''
),
(
    'Ranged Artillery Expert',
    50,
    4,
    4,
    '10: +1/+1 until end of turn',
    'Old Guard',
    'unit',
    'fighter',
    ''
),
(
    'Automated Defense System',
    80,
    6,
    6,
    'Tenacity, Reach, Defensive',
    'Old Guard',
    'unit',
    'fighter',
    ''
),
(
    'Force Hammer',
    20,
    1,
    null,
    '',
    'Neutral',
    'mech',
    null,
    ''
),
(
    'Rocket Pistol',
    30,
    2,
    null,
    '',
    'Neutral',
    'mech',
    null,
    ''
),
(  
    'Medical Supplies',
    40,
    0,
    null,
    '10: Reduce target friendly Old Guard fighter''s cooldown by 1 for each Old Guard unit it shares a lane with',
    'Old Guard',
    'mech',
    null,
    ''
),
(
    'Distracting Battlecry',
    40,
    1,
    null,
    'Deployment: All friendly Old Guard units get +0/+1 for each Old Guard unit it shares a lane with',
    'Old Guard',
    'mech',
    null,
    ''
),
(
    'Forward Battlements',
    100,
    0,
    null,
    'All creatures with Defensive lose Defensive',
    'Neutral',
    'building',
    null,
    ''
),
(
    'Giant Grower',
    10,
    1,
    1,
    'Cooldown: Target friendly fighter in this lane gets +2/+2 until end of
    turn',
    'Old Guard',
    'unit',
    'specialist',
    ''
),
(
    'Cowardly Hero',
    20,
    1,
    2,
    'Deployment: If there is a unit with 5 or more attack in this lane, gain
    +2/+2',
    'Old Guard',
    'unit',
    'fighter',
    ''
),
(
    'Behemoth Grower',
    20,
    1,
    2,
    'Cooldown: Target friendly fighter in this lane gets +1/+3 until the
    beginning of your next turn.',
    'Old Guard',
    'unit',
    'specialist',
    ''
),
(
    'Quartermaster',
    20,
    1,
    3,
    'Cooldown: All units in this lane get +1/+2 until the beginning of your
    next turn',
    'Old Guard',
    'unit',
    'specialist',
    ''
),
(
    'Short Fused Droid',
    40,
    4,
    6,
    'Shambling, When this unit takes combat damage, it gains +1 attack',
    'Old Guard',
    'unit',
    'fighter',
    ''
),
(
    'Support Marine',
    50,
    4,
    3,
    'Deployment: Gain +1/+1 for each specialist it shares a lane with',
    'Old Guard',
    'unit',
    'fighter',
    ''
),
(
    'Homesick Soldier',
    40,
    1,
    4,
    'Shambling, Deployment: If a building is active, gain +5/+2',
    'Old Guard',
    'unit',
    'specialist',
    ''
),
(
    'Military Transport',
    60,
    2,
    3,
    'At the beginning of your turn, each friendly fighter in this lane gains
    Mobility until end of turn',
    'Old Guard',
    'unit',
    'specialist',
    ''
),
(
    'Might of Explosives',
    30,
    1,
    null,
    '50: Give two target friendy Old Guard fighters +3/+3 until end of turn',
    'Old Guard',
    'mech',
    null,
    ''
),
(
    'Helm of Cooperation',
    30,
    null,
    null,
    'Deployment: All friendly Old Guard fighters gain +1/+1 
    for each specialist they share a lane with',
    'Old Guard',
    'mech',
    null,
    ''
),
(
    'Bionics Factory',
    100,
    null,
    null,
    'When built, all units in play with power 5 or greater gain Mechanized',
    'Neutral',
    'building',
    null,
    ''
),
(
    'Eager Rebel',
    10,
    2,
    1,
    'Mobility',
    'Anarchist Youth',
    'unit',
    'fighter',
    ''
),
(
    'Hoverboard Courier',
    10,
    1,
    1,
    'At the beginning of your turn, the unit with the highest attack in this
    lane gains Mobility until end of turn',
    'Anarchist Youth',
    'unit',
    'specialist',
    ''
),
(
    'Angry Junkie',
    20,
    3,
    1,
    'Shambling, Mobility',
    'Anarchist Youth',
    'unit',
    'fighter',
    ''
),
(
    'Lazy Looter',
    20,
    1,
    1,
    'Deployment: Gains +2/+1 if it is the only unit in this lane',
    'Anarchist Youth',
    'unit',
    'fighter',
    ''
),
(
    'Ignorant Brute',
    30,
    4,
    2,
    'Aggressive',
    'Anarchist Youth',
    'unit',
    'fighter',
    ''
),
(
    'Temperamental Drone',
    30,
    0,
    5,
    'Mobility, Shambling, When this unit takes combat damage, it gains +3 attack and loses mobility',
    'Anarchist Youth',
    'unit',
    'fighter',
    ''
),
(
    'Jetpack Courier',
    30,
    1,
    2,
    '20: Target unit in this lane gets +1/+0 and Mobility until end of turn',
    'Anarchist Youth',
    'unit',
    'specialist',
    ''
),
(
    'Hotwired Junker',
    40,
    4,
    7,
    'Aggressive, Shambling, When this unit takes combat damage, it loses Aggressive',
    'Anarchist Youth',
    'unit',
    'fighter',
    ''
),
(
    'Demolitions Expert',
    40,
    6,
    1,
    'At the beginning of your turn, if there is a specialist in the opposing
    lane, gain Mobility until end of turn',
    'Anarchist Youth',
    'unit',
    'fighter',
    ''
),
(
    'Makeshift Ballista',
    60,
    8,
    2,
    'Mobility, Shambling',
    'Anarchist Youth',
    'unit',
    'fighter',
    ''
),
(
    'Volatile Flamejets',
    10,
    1,
    null,
    'At the beginning of your turn, if you dont control an Anarchist Youth
    fighter, gain Debt 10',
    'Anarchist Youth',
    'mech',
    null,
    ''
),
(
    'Laser Sight',
    40,
    1,
    null,
    '20: Give two target Anarchist Youth units Mobility until end of turn',
    'Anarchist Youth',
    'mech',
    null,
    ''
),
(
    'Flanking Support Towers',
    100,
    0,
    null,
    'At the beginning of each players turns, all units in the left and right
    lane gain Aggressive',
    'Neutral',
    'building',
    null,
    ''
),
(
    'Rampaging Lunatic',
    30,
    6,
    1,
    'Berserker',
    'Anarchist Youth',
    'unit',
    'fighter',
    ''
);
    

    
    
