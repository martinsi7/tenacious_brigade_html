<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
session_start();


include("../db.php");


if(!isset($_SESSION['player']) /*|| isset($_SESSION['deck'])*/ ) {
	header("Location: ../");
	exit();
}


if (isset($_GET['isAjax']) && $_GET['isAjax']) {
	$faction = null;
	if (isset($_GET['faction'])) {
		$faction = $_GET['faction'];
	}
	$all_cards_arr = get_collection(
			$_SESSION['player']['player_id'],
			$_SESSION['player']['is_admin'],
			$faction
		);
	echo json_encode($all_cards_arr);
	exit();
}

if (isset($_POST['name']) && isset($_POST['cards'])) {
	$table_name = "Deck";
	$fields = array("player_id", "deck_name");
	$values = array(array($_SESSION['player']['player_id'], $_POST['name']));
	db_insert($table_name, $fields, $values);

	$deck_info = db_get_deck_from_name($_SESSION['player']['player_id'], $_POST['name']);
	$deck_id = $deck_info['deck_id'];
	
	$table_name = "DeckList";
	$fields = array("deck_id", "card_id");
	$values = array();
	foreach ($_POST['cards'] as $card_id) {
		array_push($values, array($deck_id, $card_id));
	}
	db_insert($table_name, $fields, $values);
	echo json_encode(array());
	exit();
}

?>

<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="../js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="../js/jquery-mobile/jquery.mobile.custom.min.js"></script>
	<script type="text/javascript" src="../js/card.js"></script>
	<script type="text/javascript" src="../js/deck.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jquery-mobile/jquery.mobile.custom.structure.min.css" media="screen and (orientation: landscape)"/>
	<link rel="stylesheet" type="text/css" href="../js/jquery-mobile/jquery.mobile.custom.theme.min.css"/>
	<link rel="stylesheet" type="text/css" href="../css/deck.css"/>
</head>
<body>
	<div class="outer-container">
		<div id="left-pane" class="pane">
			<p class="card-filter">
				<button id="show-all">Show All Cards</button>
				<button id="show-old-guard">Show Old Guard Cards</button>
				<button id="show-anarchist-youth">Show Anarchist Youth Cards</button>
				<button id="show-professionals">Show Professionals Cards</button>
			</p>
			<div class="building-cards type-container">
				<h3 class="type-title">BUILDING CARDS</h3>
				<div class="card-placement"></div>
			</div>
			<div class="mech-cards type-container">
				<h3 class="type-title">MECH CARDS</h3>
				<div class="card-placement"></div>
			</div>
			<div class="unit-cards type-container">
				<h3 class="type-title">UNIT CARDS</h3>
				<div class="card-placement"></div>
			</div>
		</div>
		<div id="right-pane" class="pane">
			name:
			<input type="text" id="deck-name">
			<button id="submit-deck" class="hide">create</button>
			<div id="total-cards" class="card-total">
				<span class="count error">0</span> /14 Total Cards
			</div>
			<div id="total-building" class="card-total">
				<span class="count error">0</span> /1 Building Cards
			</div>
			<div id="total-mech" class="card-total">
				<span class="count error">0</span> /3-6 Mech Cards
			</div>
			<div class="right-card-display"></div>
		</div>
	</div>
</body>
</html>