<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
session_start();

include("../db.php");

if(!isset($_SESSION['player']) /*|| isset($_SESSION['deck'])*/) {
	header("Location: ../");
	exit();
}

if (isset($_GET['deck_name'])) {
	$deck = db_get_deck_from_name($_SESSION['player']['player_id'], $_GET['deck_name']);
	$_SESSION['deck'] = $deck;
	header("Location: ../");
}

$deck_arr = get_decks($_SESSION['player']['player_id']);

?>

<!DOCTYPE html>
<html>
<head>
	
</head>
<body>
	<a href="build.php" class="build-deck">build a new deck</a><br/><br/>
	<?php foreach($deck_arr as $deck): ?>
		<a href="index.php?deck_name=<?php echo $deck['deck_name']; ?>" class="select-deck">
			<?php echo $deck['deck_name'] ?>
		</a><br/>
	<?php endforeach; ?>
</body>
</html>