#!/usr/local/bin/php -q
<?php
// error_reporting(E_ALL);

// /* Allow the script to hang around waiting for connections. */
// set_time_limit(0);

// /* Turn on implicit output flushing so we see what we're getting
//  * as it comes in. */
// ob_implicit_flush();

// $address = 'localhost';
// $port = 10000;

// if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
//     echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
// }

// if (socket_bind($sock, $address, $port) === false) {
//     echo "socket_bind() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
// }

// if (socket_listen($sock, 5) === false) {
//     echo "socket_listen() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
// }

// $clients = array();

// do {
//     $read = array();
//     $read[] = $sock;
//     $except = NULL;
    
//     $read = array_merge($read,$clients);
//     $write = NULL;
//     $tv_sec = 5;

//     $conn = socket_select($read,$write, $except, $tv_sec);
    
//     // Set up a blocking call to socket_select
//     if($conn < 1)
//     {
//         //    SocketServer::debug("Problem blocking socket_select?");
//         continue;
//     }
//     fwrite($conn, "yay");
//     fclose($conn);
    
//     // Handle new Connections
//     if (in_array($sock, $read)) {        
        
//         if (($msgsock = socket_accept($sock)) === false) {
//             echo "socket_accept() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
//             break;
//         }
//         $clients[] = $msgsock;
//         $key = array_keys($clients, $msgsock);
//         /* Enviar instrucciones. */
//         $msg = "\nWelcome to the PHP Test Server. \n" .
//         "You are client number: {$key[0]}\n" .
//         "To quit, type 'quit'. To shut down the server type 'shutdown'.\n";
//         socket_write($msgsock, $msg, strlen($msg));
        
//     }
    
//     // Handle Input
//     foreach ($clients as $key => $client) { // for each client        
//         if (in_array($client, $read)) {
//             if (false === ($buf = socket_read($client, 2048, PHP_NORMAL_READ))) {
//                 echo "socket_read() failed: reason: " . socket_strerror(socket_last_error($client)) . "\n";
//                 break 2;
//             }
//             echo "$buf\n";
//             if (!$buf = trim($buf)) {
//                 continue;
//             }
//             if ($buf == 'quit') {
//                 unset($clients[$key]);
//                 socket_close($client);
//                 break;
//             }
//             if ($buf == 'shutdown') {
//                 socket_close($client);
//                 break 2;
//             }
//             socket_write($client, $buf, strlen($buf));
//             $talkback = "Client {$key}: You said '$buf'.\n";
//             socket_write($client, $talkback, strlen($talkback));
//             echo "$buf\n";
//         }
        
//     }        
// } while (true);

// socket_close($sock);


// set some variables
require_once("controller.php"); // apparently useless? -Nick
$host = "localhost";
$port = 25003;
// don't timeout!
set_time_limit(0);
// create socket
$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
// bind socket to port
$result = socket_bind($socket, $host, $port) or die("Could not bind to socket\n");
// start listening for connections
$gameLogic = [];
while(1){
    $result = socket_listen($socket, 3) or die("Could not set up socket listener\n");

    // accept incoming connections
    // spawn another socket to handle communication
    $spawn = socket_accept($socket) or die("Could not accept incoming connection\n");
    // read client input
    $input = socket_read($spawn, 1024) or die("Could not read input\n");
    $output = json_encode(["result" => "fail"]);
    try {
        $output = controller::control(json_decode($input, true), $gameLogic);
        socket_write($spawn, $output, strlen ($output)) or die("Could not write output\n");
    } catch (Exception $e) {
        print_r($e);
        socket_close($spawn);
        socket_close($socket);
        break;
    }
    
}
// close sockets
socket_close($spawn);
socket_close($socket);


?>