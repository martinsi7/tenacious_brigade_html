<?php

error_reporting(E_ALL);

function send_request($params){
	// $request = json_decode($params);

	// //echo "<h2>TCP/IP Connection</h2>\n";

	// /* Get the port for the WWW service. */
	// $service_port = getservbyname('telnet', 'tcp');

	// /* Get the IP address for the target host. */
	// $address = gethostbyname('localhost');

	// /* Create a TCP/IP socket. */
	// $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
	// if ($socket === false) {
	//     echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
	// }
	// echo "connecting socket at socket: {$socket}, address: {$address}, service_port: {$service_port}";

	// //echo "Attempting to connect to '$address' on port '$service_port'...";
	// $result = socket_connect($socket, $address, 10000);
	// if ($result === false) {
	//     echo "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n";
	// }

	// // $in = "HEAD / HTTP/1.1\r\n";
	// // $in .= "Host: www.example.com\r\n";
	// // $in .= "Connection: Close\r\n\r\n";
	// // $out = '';

	// //echo "Sending HTTP HEAD request...";
	// echo $request;
	// socket_write($socket, $request, strlen($in));
	// //echo "OK.\n";

	// $result = "";
	// $output = "";
	// //echo "Reading response:\n\n";
	// while ($out = socket_read($socket, 2048)) {
	//     $result .= $out;
	// }

	// //echo "Closing socket...";
	// socket_close($socket);
	// //echo "OK.\n\n";

	$host    = "localhost";
	$port    = 25001;
	//$message = JSONConverter::php2java($params["action"], $params["data"]);
	$message = json_encode($params);
	// create socket
	$socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
	// connect to server
	$result = socket_connect($socket, $host, $port) or die("Could not connect to server\n");  
	// send string to server
	socket_write($socket, $message, strlen($message)) or die("Could not send data to server\n");
	// get server response
	try {
		$result = socket_read ($socket, 8192);
	} 
	catch (Exception $e) {
		socket_close($socket);
		echo json_encode(array(
			"action" => "fail",
			"message" => "could not get response"
		));
		exit();
	}
	// close socket
	socket_close($socket);
	return json_decode($result, true);
	//return JSONConverter::java2php($result);
}

class JSONConverter {
	public static function php2java($action, $data){
		$str = "action:" . $action;
		if ($data != "") {
			foreach ($data as $key => $value) {
				$str .= "\n{$key}:{$value}";
			}
		}
		return $str;
	}

	public static function java2php($inputString) {
		$params = explode("\n", $inputString);
		$retObj = array();
		foreach ($params as $param) {
			$pair = explode(":", $param);
			if (count($pair) == 2) {
				$retObj[$pair[0]] = $pair[1];
			}
		}
		return $retObj;
	}
}
?>